/**
 * 
 */
package com.openfitapi.mobile;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.apps.mytracks.util.ApiAdapterFactory;
import com.openfitapi.mobile.OpenFitTask.AsyncTaskController;

import static com.google.android.apps.mytracks.Constants.*;
import static com.openfitapi.mobile.OpenFitConstants.*;

/**
 * @author Jon Monroe "Berkona" Solipsis Development ©Mar 9, 2012
 */
public class OpenFitAuthenticateActivity extends Activity implements AsyncTaskController<Boolean> {

  public static final int AUTH_DIALOG = 0;
  public static final int CHECK_DIALOG = 1;

  private AlertDialog authDialog;
  private ProgressDialog authCheckDialog;

  private final String[] testingCreds = new String[3];

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    // Log.i("OpenFit", getIntent().toString());
    // Reset all shared creds;
    String action = getIntent().getAction();
    if (action != null && action.equals(Intent.ACTION_DELETE)) {
      Editor editor = getSharedPreferences(SETTINGS_NAME, MODE_PRIVATE).edit();
      editor.putString(OPENFIT_DOMAIN, "");
      editor.putString(OPENFIT_USERNAME, "");
      editor.putString(OPENFIT_ARCANUM, "");
      ApiAdapterFactory.getApiAdapter().applyPreferenceChanges(editor);
      Toast.makeText(this, R.string.openfit_auth_reset, Toast.LENGTH_LONG).show();
      finish();
    } else {
      showDialog(AUTH_DIALOG);
    }
  }

  @Override
  protected Dialog onCreateDialog(int id) {
    switch (id) {
      case AUTH_DIALOG:
        SharedPreferences prefs = getSharedPreferences(SETTINGS_NAME, MODE_PRIVATE);

        LayoutInflater factory = LayoutInflater.from(this);
        final View textEntryView = factory.inflate(R.layout.auth_dialog, null);
        String user = prefs.getString(OPENFIT_USERNAME, "");
        String domain = prefs.getString(OPENFIT_DOMAIN, "");
        String arcanum = prefs.getString(OPENFIT_ARCANUM, "");
        if ((user != null && user.length() > 0) && (domain != null && domain.length() > 0)) {
          ((EditText) textEntryView.findViewById(R.id.openfit_auth_user_edit)).setText(user + "@"
              + domain);
        }
        if (arcanum != null && arcanum.length() > 0) {
          ((EditText) textEntryView.findViewById(R.id.openfit_auth_arcanum_edit)).setText(arcanum);
        }
        authDialog = new AlertDialog.Builder(this).setIcon(R.drawable.openfit_icon)
            .setTitle(R.string.openfit_auth_dialog_title).setView(textEntryView)
            .setPositiveButton(R.string.generic_ok, new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int whichButton) {
                // This will be overridden
              }
            }).setNegativeButton(R.string.generic_cancel, new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int whichButton) {
                setResult(RESULT_CANCELED, new Intent());
                finish();
              }
            }).create();
        // You have to use this, since the default behaviour for the button
        // is to dismiss the Dialog.
        // This overrides the default AlertDialog.onClick() behaviour
        authDialog.setOnShowListener(new OnShowListener() {

          @Override
          public void onShow(DialogInterface dialog) {
            AlertDialog d = (AlertDialog) dialog;
            Button ok = d.getButton(AlertDialog.BUTTON_POSITIVE);
            ok.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View button) {

                String combined = ((EditText) authDialog.findViewById(R.id.openfit_auth_user_edit))
                    .getText().toString();

                String[] split;
                // This is not a valid username@domain pair
                if ((split = combined.split("@")).length != 2) {
                  postError(getString(R.string.openfit_user_or_domain_fail));
                  return;
                }

                testingCreds[OPENFIT_USER_IDX] = split[0];
                testingCreds[OPENFIT_DOMAIN_IDX] = split[1];

                testingCreds[OPENFIT_ARCANUM_IDX] = ((EditText) authDialog
                    .findViewById(R.id.openfit_auth_arcanum_edit)).getText().toString();

                for (int i = 0; i < testingCreds.length; i++) {
                  String s = testingCreds[i];
                  if (s.length() == 0) {
                    int errorField = Integer.MIN_VALUE;
                    switch (i) {
                      case OPENFIT_DOMAIN_IDX:
                        errorField = R.string.openfit_domain_fail;
                        break;
                      case OPENFIT_USER_IDX:
                        errorField = R.string.openfit_user_fail;
                        break;
                      case OPENFIT_ARCANUM_IDX:
                        errorField = R.string.openfit_password_fail;
                        break;
                    }
                    if (errorField != Integer.MIN_VALUE) {
                      postError(getString(errorField));
                    }
                    return;
                  }
                }
                new OpenFitAuthCheckTask(OpenFitAuthenticateActivity.this).execute(testingCreds);
              }
            });
          }

        });
        return authDialog;
      case CHECK_DIALOG:
        authCheckDialog = new ProgressDialog(this);
        authCheckDialog.setIcon(R.drawable.openfit_icon);
        authCheckDialog.setMessage(getString(R.string.openfit_auth_check_title));
        authCheckDialog.setCancelable(false);
        return authCheckDialog;
      default:
        return null;
    }
  }

  /*
   * (non-Javadoc)
   * @see com.openfitapi.mobile.OpenFitTask.AsyncTaskController#onPreExecute()
   */
  @Override
  public void onPreExecute() {
    if (authDialog.isShowing()) dismissDialog(AUTH_DIALOG);
    showDialog(CHECK_DIALOG);
  }

  /*
   * (non-Javadoc)
   * @see
   * com.openfitapi.mobile.OpenFitTask.AsyncTaskController#onProgressUpdate(int)
   */
  @Override
  public void onProgressUpdate(int progressPercent) { /* Do nothing */}

  /*
   * (non-Javadoc)
   * @see
   * com.openfitapi.mobile.OpenFitTask.AsyncTaskController#onPostExecute(java
   * .lang.Object)
   */
  @Override
  public void onPostExecute(Boolean result) {
    if (authCheckDialog.isShowing()) dismissDialog(CHECK_DIALOG);
    if (result) {
      SharedPreferences prefs = getSharedPreferences(SETTINGS_NAME, MODE_PRIVATE);

      Editor editor = prefs.edit();
      editor.putString(OPENFIT_USERNAME, testingCreds[OPENFIT_USER_IDX]);
      editor.putString(OPENFIT_DOMAIN, testingCreds[OPENFIT_DOMAIN_IDX]);
      editor.putString(OPENFIT_ARCANUM, testingCreds[OPENFIT_ARCANUM_IDX]);
      ApiAdapterFactory.getApiAdapter().applyPreferenceChanges(editor);

      resetCredentials();

      Toast.makeText(this, R.string.openfit_auth_success, Toast.LENGTH_LONG).show();

      Intent i = new Intent(this, OpenFitSendActivity.class);
      // passthrough data from original request
      i.setDataAndType(getIntent().getData(), getIntent().getType());
      i.setAction(getIntent().getAction());
      setResult(RESULT_OK, i);
      finish();
    } else {
      showDialog(AUTH_DIALOG);
      postError();
    }
  }

  private void postError() {
    postError(null);
  }

  private void postError(String errorField) {
    resetCredentials();
    ((EditText) authDialog.findViewById(R.id.openfit_auth_arcanum_edit)).setText("");

    String errorText = getString(R.string.openfit_auth_failure);
    if (errorField != null) {
      errorText += ": " + errorField;
    }
    Log.e("OpenFit", errorText);
    Toast.makeText(this, errorText, Toast.LENGTH_LONG).show();
  }

  /**
   * 
   */
  private void resetCredentials() {
    for (int i = 0; i < testingCreds.length; i++) {
      testingCreds[i] = "";
    }
  }
}
