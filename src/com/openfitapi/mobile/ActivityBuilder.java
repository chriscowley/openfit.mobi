package com.openfitapi.mobile;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class ActivityBuilder {
  private final SimpleDateFormat utc = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ",
      Locale.getDefault());
  private final SimpleDateFormat human = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
  private final NumberFormat fixedPrecision = NumberFormat.getNumberInstance(Locale.US);

  private final JSONObject delegate = new JSONObject();

  public ActivityBuilder() {
    System.out.println(Locale.getDefault());
    fixedPrecision.setGroupingUsed(false);
    fixedPrecision.setMaximumFractionDigits(5);
  }
  
  private void put(String key, String value) throws JSONException {
    if (value == null || value.equals("")) return;
    delegate.put(key, value);
  }

  public ActivityBuilder setStartTime(Date startTime) throws JSONException {
    put("start_time", utc.format(startTime));
    put("start_time_human", human.format(startTime));
    return this;
  }

  public ActivityBuilder setDuration(double seconds) throws JSONException {
    delegate.put("duration", (double)Math.round(seconds*100)/100);
    return this;
  }

  public ActivityBuilder setTitle(String name) throws JSONException {
    put("name", name);
    return this;
  }

  public ActivityBuilder setCategory(String category) throws JSONException {
    put("category", category);
    return this;
  }

  public ActivityBuilder setMaxSpeed(double maxSpeed) throws JSONException {
    delegate.put("max_speed", (double)Math.round(maxSpeed*100000)/100000);
    return this;
  }

  public ActivityBuilder setAvgSpeed(double avgSpeed) throws JSONException {
    delegate.put("avg_speed", (double)Math.round(avgSpeed*100000)/100000);
    return this;
  }

  public ActivityBuilder setTotalDistance(double distance) throws JSONException {
    delegate.put("total_distance", (double)Math.round(distance*100)/100);
    return this;
  }

  public ActivityBuilder setElevationLoss(double elevationLoss) throws JSONException {
    delegate.put("elevation_loss", (double)Math.round(elevationLoss*100)/100);
    return this;
  }

  public ActivityBuilder setElevationGain(double elevationGain) throws JSONException {
    delegate.put("elevation_gain", (double)Math.round(elevationGain*100)/100);
    return this;
  }

  public ActivityBuilder setNotes(String notes) throws JSONException {
    put("notes", notes);
    return this;
  }

  public ActivityBuilder addLocationPoint(long offset, double lat, double lng) {
    try {
      if (!delegate.has("location")) delegate.put("location", new JSONArray());
      delegate.getJSONArray("location").put(offset);
      delegate.getJSONArray("location").put(
          new JSONArray("[" + fixedPrecision.format(lat) + "," + fixedPrecision.format(lng) + "]"));
    } catch (JSONException e) {
      Log.i("OpenFit", "JSON Exception: " + e.toString());
    }
    return this;
  }

  public ActivityBuilder addElevationPoint(long offset, double elevation) throws JSONException {
    this.addTrackPoint("elevation", offset, elevation);
    return this;
  }

  public ActivityBuilder addHeartRatePoint(long offset, double heartRate) throws JSONException {
    this.addTrackPoint("heartrate", offset, heartRate);
    return this;
  }

  public ActivityBuilder addDistancePoint(long offset, double distance) throws JSONException {
    this.addTrackPoint("distance", offset, distance);
    return this;
  }

  public ActivityBuilder addCadencePoint(long offset, int cadence) throws JSONException {
    this.addTrackPoint("cadence", offset, cadence);
    return this;
  }

  public ActivityBuilder addPowerPoint(long offset, int power) throws JSONException {
    this.addTrackPoint("power", offset, power);
    return this;
  }

  private void addTrackPoint(String track, long offset, double data) throws JSONException {
    if (!delegate.has(track)) delegate.put(track, new JSONArray());
    delegate.getJSONArray(track).put(offset);
    delegate.getJSONArray(track).put((double)Math.round(data*100000)/100000);
  }

  public JSONObject build() {
    try {
      if (!delegate.has("name")) {
        StringBuilder title = new StringBuilder();
        if (delegate.has("start_time_human")) {
          title.append(delegate.getString("start_time_human"));
          if (delegate.has("type")) {
            title.append(" - ").append(delegate.getString("category"));
          }
        }
        put("name", title.toString());
      }

      delegate.remove("start_time_human");

      return delegate;
    } catch (JSONException e) {
      Log.i("OpenFit", "JSONException: " + e.toString());
      return new JSONObject();
    }
  }
}
