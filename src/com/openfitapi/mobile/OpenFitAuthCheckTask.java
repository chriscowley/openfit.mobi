/**
 * 
 */
package com.openfitapi.mobile;

import java.io.IOException;

import static com.openfitapi.mobile.OpenFitConstants.*;

/**
 * @author Jon Monroe "Berkona" Solipsis Development ©Mar 10, 2012
 */
public class OpenFitAuthCheckTask extends OpenFitTask<String, Boolean> {

  /**
   * @param owner
   */
  public OpenFitAuthCheckTask(AsyncTaskController<Boolean> owner) {
    super(owner);
  }

  /*
   * (non-Javadoc)
   * @see com.openfitapi.mobile.OpenFitTask#performTask(P[])
   */
  @Override
  protected Boolean performTask(String... params) {
    try {
      OpenFitSession testLogin = new OpenFitSession(params[OPENFIT_DOMAIN_IDX]);
      testLogin.login(params[OPENFIT_USER_IDX], params[OPENFIT_ARCANUM_IDX]);
      if (testLogin.isLoggedIn()) {
        // Valid creds
        return true;
      } else {
        // Invalid creds, reset dialog
        return false;
      }
    } catch (IOException io) {
      // It's possible that this is invalid
      // (domain error, server error, etc.) so, treat as bad
      // just in case
      return false;
    }
  }

}
