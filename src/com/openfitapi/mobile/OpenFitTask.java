package com.openfitapi.mobile;

import android.content.Context;
import android.os.AsyncTask;

/**
 * An generic adapter class that takes some of the pain out of AsyncTask. The
 * background class should implement performTask(ParamType...), and the UI
 * Thread class should implement asyncTaskController<T> with the same type as
 * the background class's ResultType.
 * 
 * @author Jon Monroe "Berkona" Solipsis Development ©Mar 10, 2012
 */
public abstract class OpenFitTask<ParamType, ResultType> extends
    AsyncTask<ParamType, Integer, ResultType> {

  protected final AsyncTaskController<ResultType> owner;

  public OpenFitTask(AsyncTaskController<ResultType> owner) {
    this.owner = owner;
  }

  @Override
  protected void onPreExecute() {
    owner.onPreExecute();
  }

  /*
   * (non-Javadoc)
   * @see android.os.AsyncTask#doInBackground(Params[])
   */
  @Override
  protected ResultType doInBackground(ParamType... params) {
    return performTask(params);
  }

  @Override
  protected void onProgressUpdate(Integer... values) {
    owner.onProgressUpdate(values[0].intValue());
  }

  @Override
  protected void onPostExecute(ResultType result) {
    owner.onPostExecute(result);
  }

  protected abstract ResultType performTask(ParamType... params);

  public interface AsyncTaskController<T> {
    public void onPreExecute();

    public void onProgressUpdate(int progressPercent);

    public void onPostExecute(T result);

    public Context getApplicationContext();
  }
}
