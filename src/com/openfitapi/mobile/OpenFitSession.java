/**
 * 
 */
package com.openfitapi.mobile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.input.BOMInputStream;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Jon Monroe "Berkona" Solipsis Development ©Mar 9, 2012
 */
public class OpenFitSession {

  private URL baseURL;

  private String session;

  public OpenFitSession(String host) throws MalformedURLException {
    setBaseURL(host);
  }

  public void setBaseURL(String host) throws MalformedURLException {
    this.baseURL = new URL("http", host, "/openfitapi/api/");
  }

  public boolean isLoggedIn() {
    return (session != null && session.length() > 0);
  }

  public void login(String username, String password) throws IOException {
    final Map<String, String> params = new HashMap<String, String>();
    params.put("username", username);
    params.put("password", password);

    JSONObject response = makeRequest("user/login", "POST", new JSONObject(params));

    try {
      session = response.getString("session_name") + "=" + response.getString("sessid");
    } catch (JSONException parseError) {
      throw new IOException("Unexpected JSON error: " + parseError.getMessage());
    }
  }

  public void logout() {
    session = null;
  }

  public JSONObject getActivityDetails(String detailURL) throws IOException {
    int trackId = Integer.valueOf(detailURL.substring(detailURL.lastIndexOf('/') + 1));
    return this.makeRequest("fitnessActivities/" + trackId, "GET");
  }

  public JSONObject createNewActivity(JSONObject activitySpec) throws IOException {
    return this.makeRequest("fitnessActivities", "POST", activitySpec, true);
  }

  public JSONObject makeRequest(String relURL, String method) throws IOException {
    return this.makeRequest(relURL, method, null);
  }

  public JSONObject makeRequest(String relURL, String method, JSONObject params) throws IOException {
    return this.makeRequest(relURL, method, params, true);
  }

  public JSONObject makeRequest(String relURL, String method, JSONObject params, boolean needsReturn)
      throws IOException {
    HttpURLConnection connection = (HttpURLConnection) new URL(baseURL, relURL).openConnection();

    if (params == null) params = new JSONObject();

    // In order to properly use finally we must declare & initialize both
    // the request writer and the response reader here
    OutputStreamWriter requestWriter = null;
    InputStream response = null;
    try {
      connection.setRequestMethod(method);
      connection.setDoInput(needsReturn);

      // Required headers for JSON services
      connection.addRequestProperty("Accept", "application/json");

      if (isLoggedIn()) {
        connection.addRequestProperty("Cookie", session);
      }

      if (params.length() > 0) {
        // Required headers for JSON services
        connection.addRequestProperty("Content-Type", "application/json");
        connection.addRequestProperty("Content-Length", String.valueOf(params.toString().length()));

        connection.setDoOutput(true);
        requestWriter = new OutputStreamWriter(connection.getOutputStream(), "UTF8");
        requestWriter.write(params.toString());
        requestWriter.flush();
      } else {
        connection.setDoOutput(false);
      }

      if (needsReturn) {
        response = connection.getInputStream();
        return parseResponse(response);
      } else {
        connection.connect();
        return null;
      }

    } catch (IOException io) {
      int responseCode = connection.getResponseCode();
      String message = connection.getResponseMessage();
      throw new IOException("Server respondeded with code: " + responseCode + " and message: "
          + message);
    } finally {
      if (response != null) {
        response.close();
      }
      connection.disconnect();
    }
  }

  /**
   * Convience method used b/c there's no easy way to read a full string from an
   * input stream
   * 
   * @param reponse
   * @return parsed JSONObject
   */
  protected JSONObject parseResponse(InputStream response) throws IOException {

    BufferedReader reader = new BufferedReader(new InputStreamReader(new BOMInputStream(response)));
    try {
      StringBuilder lineConcat = new StringBuilder();
      String line;
      while ((line = reader.readLine()) != null) {
        lineConcat.append(line);
      }
      return new JSONObject(lineConcat.toString());
    } catch (JSONException jsonErr) {
      throw new IOException("Invalid JSON Response: " + jsonErr.getMessage());
    } finally {
      reader.close();
    }
  }

  /**
   * Takes a String[] representing URL parameters and encodes it into the proper
   * http string for GET and POST data. The String[] should be organized like
   * this: {key1, value1, key2, value2} This was done to make it easier to
   * declare params, since you can use an object literal with arrays.
   * 
   * @param params
   * @return
   * @throws UnsupportedEncodingException
   */
  protected String buildQuery(String[] params) throws UnsupportedEncodingException {
    StringBuilder paramBuilder = new StringBuilder();
    for (int i = 0; i < params.length; i++) {
      String item = URLEncoder.encode(params[i], "UTF-8");
      if (i % 2 == 1) {
        paramBuilder.append('=');
      } else {
        if (i != 0) {
          paramBuilder.append('&');
        }
      }
      paramBuilder.append(item);
    }
    return paramBuilder.toString();
  }
}